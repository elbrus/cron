#!/bin/sh

WEBWML_DIR=/srv/www.debian.org/webwml
langs='arabic chinese czech dutch esperanto french german italian norwegian persian portuguese romanian russian slovak'

cd $WEBWML_DIR

case "$1" in
	init)
		echo "init not suported yet" >&2
		exit 1
		;;
	daily)
		for lang in $langs
		do
			./check_trans.pl -m -n 3 $lang
		done
		;;
	weekly)
		for lang in $langs
		do
			./check_trans.pl -m -n 2 $lang
		done
		;;
	monthly)
		for lang in $langs
		do
			./check_trans.pl -m -n 1 $lang
		done
		;;
	*)
		echo "command '$1' not supported" >&2
		exit 1
esac


